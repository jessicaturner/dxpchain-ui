# Помощь Graphene

DxpChain 2.0 - это платформа Финансовых Смарт-Контрактов, постороенная на технологии [Graphene](https://github.com/cryptonomex/graphene), созданной [Cryptonomex](http://cryptonomex.com). Graphene можно рассматривать как набор инструментов или технологию для создания блокчейнов работающих в режиме реального времени.

Этот раздел помощи дает краткий обзор и описывает основные понятия этого приложения.

## Введение

- [DxpChain](introduction/dxpchain.md)
- [Кошелек](introduction/wallets.md)
- [Резервные копии](introduction/backups.md)
- [Блокчейн](introduction/blockchain.md)
- [Делегаты](introduction/blockproducer.md)
- [Работники](introduction/benefactors.md)
- [Члены Комитета](introduction/dxpcore.md)

## Аккаунты

- [Введение](accounts/general.md)
- [Разрешения](accounts/permissions.md)
- [Членство](accounts/membership.md)

## Активы

- [Привязанные к рынку активы](assets/mpa.md) (DUSD, BitEUR, BitGOLD, Bit\*,...)
- [Эмитированные пользователем активы](assets/uia.md)
- [Приватизированные SmartTokens](assets/privsmarttokens.md)

## Децентрализованная биржа

- [Введение](dex/introduction.md)
- [Торговля](dex/trading.md)
- [Короткая продажа SmartTokens](dex/shorting.md)

* * *

[Глоссарий](glossary.md)