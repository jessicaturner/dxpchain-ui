#### Содержание

- [DxpChain](introduction/dxpchain.md)
- [Кошелек](introduction/wallets.md)
- [Резервные копии](introduction/backups.md)
- [Блокчейн](introduction/blockchain.md)
- [Голосование](voting.md)
- [Аккаунты](accounts/general.md)
- [Разрешения](accounts/permissions.md)
- [Подписки](accounts/membership.md)
- [Привязанные к рынку активы](assets/mpa.md) 
    - DUSD
    - bitEUR
    - bitGOLD
- [Эмитированные пользователем активы](assets/uia.md)
- [Приватизированные SmartTokens](assets/privsmarttokens.md)
- [Децентрализованная биржа](dex/introduction.md)
- [Торговля](dex/trading.md)
- [Короткая продажа SmartTokens](dex/shorting.md)
- [Шлюзы](gateways/introduction.md)
    - [Rudex](gateways/rudex.md)
    - [XDXPX](gateways/xdxpx.md)
