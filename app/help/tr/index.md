# Graphene Yardım

DxpChain 2.0 , [Cryptonomex](http://cryptonomex.com) tarafından inşa edilmiş olan
[Graphene](https://github.com/cryptonomex/graphene) teknolojisine dayanan bir 
Finansal-Akıllı-Kontratlar platformudur. Graphene'i  gerçek-zamanlı blokzincirleri için 
bir araç takımı yada bir teknoloji olarak da görebilirsiniz.

Bu yardım bölümü özet bir genel bakış sağlar ve bu uygulamanın en temel 
kavramlarını tanımlar.

## Takdim 
 * [DxpChain](introduction/dxpchain.md)
 * [Cüzdan](introduction/wallets.md)
 * [Yedekler](introduction/backups.md)
 * [Blokzinciri](introduction/blockchain.md)
 * [Tanıklar](introduction/blockproducer.md)
 * [İşçiler](introduction/benefactors.md)
 * [Kurul Üyeleri](introduction/dxpcore.md)

## Hesaplar
 * [Giriş](accounts/general.md)
 * [İzinler](accounts/permissions.md)
 * [Üyelikler](accounts/membership.md)

## Aktifler
 * [Piyasa Sabitli Varlıklar](assets/mpa.md) (DUSD, BitEUR, BitGOLD, Bit\*,...)
 * [Kullanıcı Aktifleri](assets/uia.md)
 * [Özelleştirilmiş Varlıklar](assets/privsmarttokens.md)

## Merkezsiz/Dağıtık Borsa
 * [Giriş](dex/introduction.md)
 * [Alış/Satış](dex/trading.md)
 * [BitAktif Kısa Satışları](dex/shorting.md)

----------
[Terimler](glossary.md)