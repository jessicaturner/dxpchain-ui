# Xdxpx Gateway Service

Xdxpx is a gateway service built on the Dxpchain Exchange. A gateway service is responsible for moving cryptocurrencies to and from the Dxpchain Exchange. They support a wide range of popular assets. You can easily identify those supported by XdxpX because they are prefixed with the word XDXPX.*. For example XDXPX.STH, XDXPX.POST etc.

## Website
[XDXP.io](https://xdxp.io)

## Support
- [Telegram Chat](https://t.me/xdxpio)
