# Help

This is the reference wallet of DxpChain Blockchain which connects to DxpChain Blockchain nodes (dxpchain-core).

This help section gives a brief overview and describes the most basic concepts
of this application.

## Introduction 
 * [DxpChain](introduction/dxpchain.md)
 * [Wallet](introduction/wallets.md)
 * [Backups](introduction/backups.md)
 * [Blockchain](introduction/blockchain.md)
 * [Blockproducers](introduction/blockproducer.md)
 * [benefactors](introduction/benefactors.md)
 * [Committe Members](introduction/dxpcore.md)

## Accounts
 * [Introduction](accounts/general.md)
 * [Permissions](accounts/permissions.md)
 * [Memberships](accounts/membership.md)

## Assets
 * [Market Pegged Assets](assets/mpa.md) (DUSD, BitEUR, BitGOLD, Bit\*,...)
 * [User Issued Assets](assets/uia.md)
 * [Privatized SmartTokens](assets/privsmarttokens.md)

## Decentralized Exchange
 * [Introduction](dex/introduction.md)
 * [Trading](dex/trading.md)
 * [Short Selling SmartTokens](dex/shorting.md)

## Development
 * [Dxpchain UI Github](https://gitlab.com/jessicaturner/dxpchain-ui)

----------
[Glossary](glossary.md)
