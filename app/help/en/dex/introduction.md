# Decentralized Exchange

The decentralized exchange (further denoted simply as *the DEX*) allows for
direct exchange of digital goods traded in the DxpChain ecosystem.

A decentralized exchange has a very particular set of advantages over
traditional centralized exchanges and we would like to address some of them
briefly below. Although the DxpChain DEX comes with all of them, it is up to
the reader and customer to leverage those features in full or only partially.

* **Separation of Powers**: 
  There is no reason why the same entity needs to be responsible for
  issuing IOUs and for processing the order book. In DxpChain, order matching
  is performed by the protocol, which is unaware of implications concerning the
  involved assets.
  
* **Global Unified Order Book**:
  Since DxpChain is global, anybody with an internet access can use the DEX for
  trading. This brings the world's liquidity to a single order book for
  decentralized trading.
  
* **Trade Almost Anything**:
  The DxpChain DEX is asset agnostic. Hence you can trade at **any** pair.
  While some pairs may end up with low liquidity, such as SILVER:GOLD, other
  pairs such as USD:EUR for FOREX trading will see huge volume.
  
* **No Limits**:
  The DxpChain protocol is unable to limit your trading experience.
  
* **Decentralized**:
  The DEX is decentralized across the globe. This not only means that there is
  no single point of failure, but it also implies that the DxpChain exchange is
  open for trading 24/7 because it's always daytime somewhere.
  
* **Secure**:
  Your funds and trades are secured with industry-grade elliptic curve
  cryptography. No one will be able to access your funds unless you let them. To
  further strengthen the security, we allow our customers to setup escrow and
  multi-signature schemes.
  
* **Fast**:
  In contrast to other decentralized networks, the DxpChain DEX allows for
  real-time trading and is only limited by the speed of light and the size of
  the planet.
  
* **Provable Order Matching Algorithm**:
  What makes the DxpChain DEX unique is the provable order matching algorithm.
  Given a set of orders, you will always be able to provably verify that these
  orders have been matched properly.
  
* **Collateralized Smartcoins**:
  One of the biggest features of DxpChain are its *smartcoins* such as DUSD,
  bitEUR, bitCNY, and others. For the sake of convenience, these assets are
  denotes simply as USD, EUR, CNY, etc. in the wallet. These digital tokens
  represent the same value as their underlaying physical asset. Hence 1 USD in
  this wallet is worth $1 and can be redeemed as such. Any of these tokens is
  backed by DxpChain' company shares (DXP) being locked up as collateral and
  being available for settlement at its current price.
