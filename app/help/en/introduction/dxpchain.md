# DxpChain
This is the reference wallet of DxpChain Blockchain which connects to DxpChain Blockchain nodes (dxpchain-core).

This reference wallet is a browser based wallet. Your keys are stored locally in your browser and never leave it as transactions are signed locally 
before broadcasting them. Never expose your keys to anyone. 

## Support

>If you have an issue related to a Gateway provider, please be sure to contact them directly. See Gateways in the left menu.

### DxpChain Chat
You can chat with other member of the DxpChain community in real time at:

- [DxpChainDAC](https://t.me/DxpChainDAC)

If you seek support for this wallet, there are community members offering that in Telegram:

- https://t.me/dxpWalletHelp

### Developers
If you are a software developer, you may contribute to the DxpChain Blockchain via:

- [Github UI](https://gitlab.com/jessicaturner/dxpchain-ui)
- [Github Core](https://github.com/dxpchain/dxpchain-core) 
- https://t.me/DxpChainDev

## Introduction
Welcome to the worlds fastest decentralized exchange (DEX).

DxpChain looks to extend the innovation of the blockchain to all industries
that rely upon the internet to provide their services. Whether its banking,
stock exchanges, lotteries, voting, music, auctions or many others, a digital
public ledger allows for the creation of distributed autonomous companies (or
DACs) that provide better quality services at a fraction of the cost incurred by
their more traditional, centralized counterparts. The advent of DACs ushers in a
new paradigm in organizational structure in which companies can run without any
human management and under the control of an incorruptible set of business
rules. These rules are encoded in publicly auditable open source software
distributed across the computers of the companies’ shareholders, who
effortlessly secure the company from arbitrary control.

DxpChain does for business what bitcoin did for money by utilizing distributed
consensus technology to create companies that are inherently global,
transparent, trustworthy, efficient and most importantly profitable.

DxpChain Blockchain has enabled several financial innovations such as: User Issued Asset (UIA), Market Pegged Asset (MPA), Non-Fungible Tokens (NFT), Decentralized Exchange (DEX), Automated Market Maker (AMM) and Hashed Time-lock Contract (HTLC).

## Wallet
This wallet allows access most features of the DxpChain network including:

- [SmartTokens](/help/assets/mpa)
- [User-Issued Assets](/help/assets/uia)
- [The DEX](/help/dex/introduction)

The main focuses for the design of DxpChain Blockchain Reference wallet and DxpChain Blockchain are:

- No custody of core cryptocurrency funds: traders maintain control of their own private keys and core cryptocurrency funds.
- High performance: low latency, high throughput for a large user base, and high ability of liquidity trading.
- Low cost of transaction: in both transaction fees and liquidity trading cost.
- Fair trading features: transparency for all.
- Evolving: able to develop with forever-improving technology stack, architecture, and ideas.
