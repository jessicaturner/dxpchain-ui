//Unknown asset
require("file-loader?name=asset-symbols/[name].png!./unknown.png");

// Core asset
require("file-loader?name=asset-symbols/[name].png!./dxp.png");

// smartassets
require("file-loader?name=asset-symbols/[name].png!./dstar.png");
require("file-loader?name=asset-symbols/[name].png!./page.png");
require("file-loader?name=asset-symbols/[name].png!./comp.png");
require("file-loader?name=asset-symbols/[name].png!./proj.png");
require("file-loader?name=asset-symbols/[name].png!./grup.png");
require("file-loader?name=asset-symbols/[name].png!./jobs.png");
require("file-loader?name=asset-symbols/[name].png!./news.png");
require("file-loader?name=asset-symbols/[name].png!./poll.png");
require("file-loader?name=asset-symbols/[name].png!./evnt.png");
require("file-loader?name=asset-symbols/[name].png!./reso.png");
require("file-loader?name=asset-symbols/[name].png!./annt.png");
require("file-loader?name=asset-symbols/[name].png!./advt.png");
require("file-loader?name=asset-symbols/[name].png!./srvc.png");

// smartcurrencies
require("file-loader?name=asset-symbols/[name].png!./dusd.png");
require("file-loader?name=asset-symbols/[name].png!./rub.png");
require("file-loader?name=asset-symbols/[name].png!./gbp.png");
require("file-loader?name=asset-symbols/[name].png!./cny.png");
require("file-loader?name=asset-symbols/[name].png!./eur.png");
require("file-loader?name=asset-symbols/[name].png!./jpy.png");
//require("file-loader?name=asset-symbols/[name].png!./krw.png");
require("file-loader?name=asset-symbols/[name].png!./inr.png");
require("file-loader?name=asset-symbols/[name].png!./chf.png");
require("file-loader?name=asset-symbols/[name].png!./cad.png");
require("file-loader?name=asset-symbols/[name].png!./hkd.png");
require("file-loader?name=asset-symbols/[name].png!./aud.png");
require("file-loader?name=asset-symbols/[name].png!./mxn.png");
require("file-loader?name=asset-symbols/[name].png!./sgd.png");
require("file-loader?name=asset-symbols/[name].png!./idr.png");
require("file-loader?name=asset-symbols/[name].png!./sek.png");
require("file-loader?name=asset-symbols/[name].png!./nzd.png");
require("file-loader?name=asset-symbols/[name].png!./aed.png");
require("file-loader?name=asset-symbols/[name].png!./thb.png");
require("file-loader?name=asset-symbols/[name].png!./mop.png");

// 3rd party assets
//require("file-loader?name=asset-symbols/[name].png!./knk.png");
//require("file-loader?name=asset-symbols/[name].png!./fsho.png");
//require("file-loader?name=asset-symbols/[name].png!./jnm.png");
//require("file-loader?name=asset-symbols/[name].png!./dxk.png");
require("file-loader?name=asset-symbols/[name].png!./ada.png");
require("file-loader?name=asset-symbols/[name].png!./bnb.png");
require("file-loader?name=asset-symbols/[name].png!./btc.png");
require("file-loader?name=asset-symbols/[name].png!./dot.png");
require("file-loader?name=asset-symbols/[name].png!./eos.png");
require("file-loader?name=asset-symbols/[name].png!./eth.png");
require("file-loader?name=asset-symbols/[name].png!./sol.png");
require("file-loader?name=asset-symbols/[name].png!./trx.png");
require("file-loader?name=asset-symbols/[name].png!./usdt.png");
require("file-loader?name=asset-symbols/[name].png!./xlm.png");
require("file-loader?name=asset-symbols/[name].png!./xrp.png");