import {Apis} from "dxpchainjs-ws";
/** This file centralized customization and branding efforts throughout the whole wallet and is meant to facilitate
 *  the process.
 *
 *  @author Stefan Schiessl <stefan.schiessl@blockchainprojectsbv.com>
 */

/**
 * Determine if we are running on testnet or mainnet
 * @private
 */
function _isTestnet() {
    const testnet =
        //"39f5e2ede1f8bc1a3a54a7914414e3779e33193f1f5693510e73cb7a87617447"; // just for the record
        "71a4332ce31af65a9f8b9503053644166754fab70ff0b559fbed8be8d0dcb6e2";
    const mainnet =
        //"4018d7844c78f6a6c41c6a552b898022310fc5dec06da467ee7905a8dad512c8";
        "71a4332ce31af65a9f8b9503053644166754fab70ff0b559fbed8be8d0dcb6e2";

    // treat every other chain as testnet
    return Apis.instance().chain_id !== mainnet;
}

/**
 * Wallet name that is used throughout the UI and also in translations
 * @returns {string}
 */
export function getWalletName() {
    return "DxpChain";
}

/**
 * URL of this wallet
 * @returns {string}
 */
export function getWalletURL() {
    return "https://wallet.gvxlive.com"; //"ws://wallet.gvxlive.org";
}

/**
 * Returns faucet information
 *
 * @returns {{url: string, show: boolean}}
 */
export function getFaucet() {
    return {
        url: "https://faucet.gvxlive.com",
        show: true,
        editable: false,
        referrer: "dxpchain-faucet"
    };
}

export function getTestFaucet() {
    // fixme should be solved by introducing _isTestnet into getFaucet and fixing the mess in the Settings when fetching faucet address
    return {
        url: "wss://faucet.gvxlive.com", //"https://faucet.testnet.dxpchain.eu", // operated as a contribution by DxpChain EU
        show: true,
        editable: false
    };
}

/**
 * Logo that is used throughout the UI
 * @returns {*}
 */
export function getLogo() {
    return require("assets/logo-ico-blue.png").default;
}

/**
 * Default set theme for the UI
 * @returns {string}
 */
export function getDefaultTheme() {
    // possible ["darkTheme", "lightTheme", "midnightTheme"]
    return "darkTheme";
}

/**
 * Default login method. Either "password" (for cloud login mode) or "wallet" (for local wallet mode)
 * @returns {string}
 */
export function getDefaultLogin() {
    // possible: one of "password", "wallet"
    return "password";
}

/**
 * Default units used by the UI
 *
 * @returns {[string,string,string,string,string,string]}
 */
export function getUnits() {
    if (_isTestnet()) {
        return ["TEST"];
    }
    return [
        "DXP",
        "USD",
        "RUB",
        "GBP",
        "CNY",
        "EUR",
        "JPY",
        "KRW",
        "INR",
        "CHF",
        "CAD",
        "HKD",
        "AUD",
        "MXN",
        "SGD",
        "IDR",
        "SEK",
        "NZD",
        "AED",
        "THB",
        "MOP"
    ];
}

export function getDefaultMarket() {
    if (_isTestnet()) {
        return "USD_TEST";
    }
    return "DXP_CNY";
}

/**
 * These are the highlighted bases in "My Markets" of the exchange
 *
 * @returns {[string]}
 */
export function getMyMarketsBases() {
    if (_isTestnet()) {
        return ["TEST"];
    }
    return ["DXP", "DSTAR", "DUSD"];
}

/**
 * These are the default quotes that are shown after selecting a base
 *
 * @returns {[string]}
 */
export function getMyMarketsQuotes() {
    if (_isTestnet()) {
        return ["TEST"];
    }
    let tokens = {
        nativeTokens: [
            "DXP",
            "USD",
            "RUB",
            "GBP",
            "CNY",
            "EUR",
            "JPY",
            "KRW",
            "INR",
            "CHF",
            "CAD",
            "HKD",
            "AUD",
            "MXN",
            "SGD",
            "IDR",
            "SEK",
            "NZD",
            "AED",
            "THB",
            "MOP"
        ],
        otherTokens: ["CVCOIN", "HERO", "OCT", "HERTZ", "YOYOW"]
        // honestTokens: ["HONEST.BTC", "HONEST.USD"]
    };

    let allTokens = [];
    for (let type in tokens) {
        allTokens = allTokens.concat(tokens[type]);
    }
    return allTokens;
}

/**
 * The featured markets displayed on the landing page of the UI
 *
 * @returns {list of string tuples}
 */
export function getFeaturedMarkets(quotes = []) {
    if (_isTestnet()) {
        return [["USD", "TEST"]];
    }
    return [
        ["DXP", "D.USD"],
        ["DXP", "D.STAR"],
        ["DXP", "D.PAGE"],
        ["DXP", "D.COMP"],
        ["DXP", "D.PROJ"],
        ["DXP", "D.GRUP"],
        ["DXP", "D.JOBS"],
        ["DXP", "D.NEWS"],
        ["DXP", "D.POLL"],
        ["DXP", "D.EVNT"],
        ["DXP", "D.RESO"],
        ["DXP", "D.ANNT"],
        ["DXP", "D.ADVT"],
        ["DXP", "D.SRVC"],
        ["DXP", "GVX.USDT"],
        ["DXP", "GVX.ETH"],
        ["DXP", "GVX.EOS"],
        ["DXP", "GVX.XRP"],
        ["DXP", "D.RUB"],
        ["DXP", "D.GBP"],
        ["DXP", "D.CNY"],
        ["DXP", "D.EUR"],
        ["DXP", "D.JPY"],
        ["DXP", "D.KRW"],
        ["DXP", "D.INR"],
        ["DXP", "D.CHF"],
        ["DXP", "D.CAD"],
        ["DXP", "D.HKD"],
        ["DXP", "D.AUD"],
        ["DXP", "D.MXN"],
        ["DXP", "D.SGD"],
        ["DXP", "D.IDR"],
        ["DXP", "D.HKD"],
        ["DXP", "D.SEK"],
        ["DXP", "D.NZD"],
        ["DXP", "D.AED"],
        ["DXP", "D.THB"],
        ["DXP", "D.MOP"],
        ["D.STAR", "DXP"],
        ["D.STAR", "D.USD"],
        ["D.STAR", "D.PAGE"],
        ["D.STAR", "D.COMP"],
        ["D.STAR", "D.PROJ"],
        ["D.STAR", "D.GRUP"],
        ["D.STAR", "D.JOBS"],
        ["D.STAR", "D.NEWS"],
        ["D.STAR", "D.POLL"],
        ["D.STAR", "D.EVNT"],
        ["D.STAR", "D.RESO"],
        ["D.STAR", "D.ANNT"],
        ["D.STAR", "D.ADVT"],
        ["D.STAR", "D.SRVC"],
        ["D.STAR", "GVX.USDT"],
        ["D.STAR", "GVX.ETH"],
        ["D.STAR", "GVX.EOS"],
        ["D.STAR", "GVX.XRP"],
        ["D.STAR", "D.RUB"],
        ["D.STAR", "D.GBP"],
        ["D.STAR", "D.CNY"],
        ["D.STAR", "D.EUR"],
        ["D.STAR", "D.JPY"],
        ["D.STAR", "D.KRW"],
        ["D.STAR", "D.INR"],
        ["D.STAR", "D.CHF"],
        ["D.STAR", "D.CAD"],
        ["D.STAR", "D.HKD"],
        ["D.STAR", "D.AUD"],
        ["D.STAR", "D.MXN"],
        ["D.STAR", "D.SGD"],
        ["D.STAR", "D.IDR"],
        ["D.STAR", "D.HKD"],
        ["D.STAR", "D.SEK"],
        ["D.STAR", "D.NZD"],
        ["D.STAR", "D.AED"],
        ["D.STAR", "D.THB"],
        ["D.STAR", "D.MOP"],
        ["D.USD", "DXP"],
        ["D.USD", "D.STAR"],
        ["D.USD", "D.PAGE"],
        ["D.USD", "D.COMP"],
        ["D.USD", "D.PROJ"],
        ["D.USD", "D.GRUP"],
        ["D.USD", "D.JOBS"],
        ["D.USD", "D.NEWS"],
        ["D.USD", "D.POLL"],
        ["D.USD", "D.EVNT"],
        ["D.USD", "D.RESO"],
        ["D.USD", "D.ANNT"],
        ["D.USD", "D.ADVT"],
        ["D.USD", "D.SRVC"],
        ["D.STAR", "GVX.ETH"],
        ["D.STAR", "GVX.EOS"],
        ["D.STAR", "GVX.XRP"],
        ["D.USD", "D.RUB"],
        ["D.USD", "D.GBP"],
        ["D.USD", "D.CNY"],
        ["D.USD", "D.EUR"],
        ["D.USD", "D.JPY"],
        ["D.USD", "D.KRW"],
        ["D.USD", "D.INR"],
        ["D.USD", "D.CHF"],
        ["D.USD", "D.CAD"],
        ["D.USD", "D.HKD"],
        ["D.USD", "D.AUD"],
        ["D.USD", "D.MXN"],
        ["D.USD", "D.SGD"],
        ["D.USD", "D.IDR"],
        ["D.USD", "D.HKD"],
        ["D.USD", "D.SEK"],
        ["D.USD", "D.NZD"],
        ["D.USD", "D.AED"],
        ["D.USD", "D.THB"],
        ["D.USD", "D.MOP"]
    ].filter(a => {
        if (!quotes.length) return true;
        return quotes.indexOf(a[0]) !== -1;
    });
}

/**
 * Recognized namespaces of assets
 *
 * @returns {[string,string,string,string,string,string,string]}
 */
export function getAssetNamespaces() {
    if (_isTestnet()) {
        return [];
    }
    return ["D.", "GVX."];
}

/**
 * These namespaces will be hidden to the user, this may include "bit" for SmartTokens
 * @returns {[string,string]}
 */
export function getAssetHideNamespaces() {
    // e..g "XDXPX.", "bit"
    return [];
}

/**
 * Allowed gateways that the user will be able to choose from in Deposit Withdraw modal
 * @param gateway
 * @returns {boolean}
 */
export function allowedGateway(gateway) {
    const allowedGateways = [
        "GVX"
        //"TRADE",
        //"OPEN", // keep to display the warning icon, permanently disabled in gateways.js
        //"RUDEX", // keep to display the warning icon, permanently disabled in gateways.js
        //"GDEX",
        //"XDXPX",
        //"IOB",
        //"CITADEL", // keep to display the warning icon, permanently disabled in gateways.js
        //"BRIDGE", // keep to display the warning icon, permanently disabled in gateways.js
        //"SPARKDEX" // keep to display the warning icon, permanently disabled in gateways.js
    ];
    if (!gateway) {
        // answers the question: are any allowed?
        return allowedGateways.length > 0;
    }
    return allowedGateways.indexOf(gateway) >= 0;
}

export function getSupportedLanguages() {
    // not yet supported
}

export function getAllowedLogins() {
    // possible: list containing any combination of ["password", "wallet"]
    return ["password", "wallet"];
}

export function getConfigurationAsset() {
    let assetSymbol = null;
    if (_isTestnet()) {
        assetSymbol = "NOTIFICATIONS";
    } else {
        assetSymbol = "TEST";
    }
    // explanation will be parsed out of the asset description (via split)
    return {
        symbol: assetSymbol,
        explanation:
            "This asset is used for decentralized configuration of the DxpChain UI placed under dxpchain.org."
    };
}

export function getSteemNewsTag() {
    return null;
}
