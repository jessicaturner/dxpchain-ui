import React from "react";
import Translate from "react-translate-component";
import benefactorsList from "../benefactorsList";
import {Link} from "react-router-dom";
import AssetName from "../../Utility/AssetName";
import counterpart from "counterpart";
import {EquivalentValueComponent} from "../../Utility/EquivalentValueComponent";
import FormattedAsset from "../../Utility/FormattedAsset";
import {
    Row,
    Col,
    Radio,
    Input,
    Icon as AntIcon,
    Button
} from "dxpchain-ui-style-guide";
import SearchInput from "../../Utility/SearchInput";

export default class benefactors extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            newbenefactorsLength: null,
            activebenefactorsLength: null,
            pollsLength: null,
            expiredbenefactorsLength: null,
            voteThreshold: null,
            benefactorTableIndex: props.viewSettings.get("benefactorTableIndex", 1)
        };
    }

    shouldComponentUpdate(np, ns) {
        return (
            ns.newbenefactorsLength !== this.state.newbenefactorsLength ||
            ns.activebenefactorsLength !== this.state.activebenefactorsLength ||
            ns.pollsLength !== this.state.pollsLength ||
            ns.expiredbenefactorsLength !== this.state.expiredbenefactorsLength ||
            ns.voteThreshold !== this.state.voteThreshold ||
            np.benefactorTableIndex !== this.state.benefactorTableIndex
        );
    }

    render() {
        const {
            vote_ids,
            proxy_vote_ids,
            hideLegacy,
            preferredUnit,
            globalObject,
            totalBudget,
            benefactorBudget,
            hideLegacyProposals,
            hasProxy,
            filterSearch,
            onFilterChange,
            onChangeVotes,
            getbenefactorArray
        } = this.props;

        const {
            benefactorTableIndex,
            newbenefactorsLength,
            activebenefactorsLength,
            pollsLength,
            expiredbenefactorsLength,
            voteThreshold
        } = this.state;

        const setbenefactorTableIndex = e => {
            this.setState({
                benefactorTableIndex: e.target.value
            });
        };

        // fixme the way benefactorsList is injected with benefactorslist is a complete design fail. use proper controlled component

        return (
            <div>
                <div className="header-selector">
                    <div style={{float: "right"}}>
                        <Button>
                            <Link to="/create-benefactor">
                                <Translate content="account.votes.create_benefactor" />
                            </Link>
                        </Button>
                    </div>

                    <div className="selector inline-block">
                        <SearchInput
                            placeholder={counterpart.translate(
                                "explorer.blockproducers.filter_by_name"
                            )}
                            value={filterSearch}
                            onChange={onFilterChange}
                        />
                        <Radio.Group
                            defaultValue={1}
                            onChange={setbenefactorTableIndex}
                        >
                            <Radio value={0}>
                                {counterpart.translate("account.votes.new", {
                                    count: newbenefactorsLength
                                })}
                            </Radio>

                            <Radio value={1}>
                                {counterpart.translate("account.votes.active", {
                                    count: activebenefactorsLength
                                })}
                            </Radio>

                            {pollsLength ? (
                                <Radio value={3}>
                                    {counterpart.translate(
                                        "account.votes.polls",
                                        {count: pollsLength}
                                    )}
                                </Radio>
                            ) : null}

                            {expiredbenefactorsLength ? (
                                <Radio value={2}>
                                    <Translate content="account.votes.expired" />
                                </Radio>
                            ) : null}
                        </Radio.Group>
                    </div>

                    {hideLegacy}
                    <br />
                    <br />
                    <Row>
                        <Col span={3}>
                            <Translate content="account.votes.threshold" /> (
                            <AssetName name={preferredUnit} />)
                        </Col>
                        <Col
                            span={3}
                            style={{
                                marginLeft: "10px"
                            }}
                        >
                            <FormattedAsset
                                decimalOffset={4}
                                hide_asset
                                amount={voteThreshold}
                                asset="1.3.0"
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col span={3}>
                            <Translate content="account.votes.total_budget" /> (
                            <AssetName name={preferredUnit} />)
                        </Col>
                        <Col
                            span={3}
                            style={{
                                marginLeft: "10px"
                            }}
                        >
                            {globalObject ? (
                                <EquivalentValueComponent
                                    hide_asset
                                    fromAsset="1.3.0"
                                    toAsset={preferredUnit}
                                    amount={totalBudget}
                                />
                            ) : null}
                        </Col>
                    </Row>
                </div>
                <benefactorsList
                    benefactorTableIndex={benefactorTableIndex}
                    preferredUnit={preferredUnit}
                    setbenefactorsLength={(
                        _newbenefactorsLength,
                        _activebenefactorsLength,
                        _pollsLength,
                        _expiredbenefactorsLength,
                        _voteThreshold
                    ) => {
                        if (
                            _newbenefactorsLength !== this.state.newbenefactorsLength ||
                            _activebenefactorsLength !==
                                this.state.activebenefactorsLength ||
                            _pollsLength !== this.state.pollsLength ||
                            _expiredbenefactorsLength !==
                                this.state.expiredbenefactorsLength ||
                            _voteThreshold !== this.state.voteThreshold
                        ) {
                            this.setState({
                                newbenefactorsLength: _newbenefactorsLength,
                                activebenefactorsLength: _activebenefactorsLength,
                                pollsLength: _pollsLength,
                                expiredbenefactorsLength: _expiredbenefactorsLength,
                                voteThreshold: _voteThreshold
                            });
                        }
                    }}
                    benefactorBudget={benefactorBudget}
                    hideLegacyProposals={hideLegacyProposals}
                    hasProxy={hasProxy}
                    proxy_vote_ids={proxy_vote_ids}
                    vote_ids={vote_ids}
                    onChangeVotes={onChangeVotes}
                    getbenefactorArray={getbenefactorArray}
                    filterSearch={filterSearch}
                />
            </div>
        );
    }
}
