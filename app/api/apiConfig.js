import {getFaucet, getTestFaucet} from "../branding";

export const ioxbankAPIs = {
    BASE: "https://api.ioxbank.com/dxpchain",
    COINS_LIST: "/coins",
    ACTIVE_WALLETS: "/active-wallets",
    TRADING_PAIRS: "/trading-pairs",
    NEW_DEPOSIT_ADDRESS: "/simple-api/initiate-trade"
};

export const blockTradesAPIs = {
    BASE: "https://api.blocktrades.us/v2",
    COINS_LIST: "/coins",
    ACTIVE_WALLETS: "/active-wallets",
    TRADING_PAIRS: "/trading-pairs",
    DEPOSIT_LIMIT: "/deposit-limits",
    ESTIMATE_OUTPUT: "/estimate-output-amount",
    ESTIMATE_INPUT: "/estimate-input-amount"
};

export const openledgerAPIs = {
    BASE: "https://ol-api1.openledger.info/api/v0/ol/support",
    COINS_LIST: "/coins",
    ACTIVE_WALLETS: "/active-wallets",
    TRADING_PAIRS: "/trading-pairs",
    DEPOSIT_LIMIT: "/deposit-limits",
    ESTIMATE_OUTPUT: "/estimate-output-amount",
    ESTIMATE_INPUT: "/estimate-input-amount",
    RPC_URL: "https://openledger.info/api/"
};

export const rudexAPIs = {
    BASE: "https://gateway.rudex.org/api/rudex",
    COINS_LIST: "/coins",
    NEW_DEPOSIT_ADDRESS: "/simple-api/initiate-trade"
};

export const bitsparkAPIs = {
    BASE: "https://dex-api.bitspark.io/api/v1",
    COINS_LIST: "/coins",
    ACTIVE_WALLETS: "/active-wallets",
    TRADING_PAIRS: "/trading-pairs",
    DEPOSIT_LIMIT: "/deposit-limits",
    ESTIMATE_OUTPUT: "/estimate-output-amount",
    ESTIMATE_INPUT: "/estimate-input-amount"
};

export const gvxAPIs = {
    BASE: "https://gvxlive.org/",
    COINS_LIST: "/coins",
    ACTIVE_WALLETS: "/active-wallets",
    TRADING_PAIRS: "/trading-pairs",
    DEPOSIT_LIMIT: "/deposit-limits",
    ESTIMATE_OUTPUT: "/estimate-output-amount",
    ESTIMATE_INPUT: "/estimate-input-amount"
};

export const cryptoBridgeAPIs = {
    BASE: "https://api.crypto-bridge.org/api/v1",
    COINS_LIST: "/coins",
    ACTIVE_WALLETS: "/wallets",
    MARKETS: "/markets",
    TRADING_PAIRS: "/trading-pairs"
};

export const citadelAPIs = {
    BASE: "https://citadel.li/trade",
    COINS_LIST: "/coins",
    ACTIVE_WALLETS: "/active-wallets",
    TRADING_PAIRS: "/trading-pairs",
    DEPOSIT_LIMIT: "/deposit-limits",
    ESTIMATE_OUTPUT: "/estimate-output-amount",
    ESTIMATE_INPUT: "/estimate-input-amount"
};

export const gdex2APIs = {
    BASE: "https://api.52dxp.net/adjust",
    COINS_LIST: "/coins",
    ACTIVE_WALLETS: "/active-wallets",
    TRADING_PAIRS: "/trading-pairs"
};

// Legacy Deposit/Withdraw
export const gdexAPIs = {
    BASE: "https://api.52dxp.net",
    ASSET_LIST: "/gateway/asset/assetList",
    ASSET_DETAIL: "/gateway/asset/assetDetail",
    GET_DEPOSIT_ADDRESS: "/gateway/address/getAddress",
    CHECK_WITHDRAY_ADDRESS: "/gateway/address/checkAddress",
    DEPOSIT_RECORD_LIST: "/gateway/deposit/recordList",
    DEPOSIT_RECORD_DETAIL: "/gateway/deposit/recordDetail",
    WITHDRAW_RECORD_LIST: "/gateway/withdraw/recordList",
    WITHDRAW_RECORD_DETAIL: "/gateway/withdraw/recordDetail",
    GET_USER_INFO: "/gateway/user/getUserInfo",
    USER_AGREEMENT: "/gateway/user/isAgree",
    WITHDRAW_RULE: "/gateway/withdraw/rule"
};

export const xdxpxAPIs = {
    BASE: "https://apis.xdxp.io/api/v1",
    COINS_LIST: "/coin"
};

export const nodeRegions = [
    // region of the node follows roughly https://en.wikipedia.org/wiki/Subregion#/media/File:United_Nations_geographical_subregions.png
    "Northern Europe",
    "Western Europe",
    "Southern Europe",
    "Eastern Europe",
    "Northern Asia",
    "Western Asia",
    "Southern Asia",
    "Eastern Asia",
    "Central Asia",
    "Southeastern Asia",
    "Australia and New Zealand",
    "Melanesia",
    "Polynesia",
    "Micronesia",
    "Northern Africa",
    "Western Africa",
    "Middle Africa",
    "Eastern Africa",
    "Southern Africa",
    "Northern America",
    "Central America",
    "Caribbean",
    "South America"
];

export const settingsAPIs = {
    // If you want a location to be translated, add the translation to settings in locale-xx.js
    // and use an object {translate: key} in WS_NODE_LIST
    DEFAULT_WS_NODE: "wss://node.gvxlive.com",
    WS_NODE_LIST: [
        {
            url: "wss://node.gvxlive.com",
            location: "India"
        }
        // {
        //     url: "wss://fake.automatic-selection.com",
        //     location: {translate: "settings.api_closest"}
        // },
        // {
        //     url: "ws://127.0.0.1:8090",
        //     location: "Locally hosted"
        // },
        // {
        //     url: "wss://nexus01.co.uk/ws",
        //     region: "Northern Europe",
        //     country: "England",
        //     location: "Gloucester",
        //     operator: "Blockproducer: nexus01",
        //     contact: "telegram:rosswlkr"
        // },
        // {
        //     url: "wss://dex.iobanker.com/ws",
        //     region: "Western Europe",
        //     country: "Germany",
        //     location: "Frankfurt",
        //     operator: "Blockproducer: iobanker-core",
        //     contact: "email:admin@iobanker.com"
        // },
        // {
        //     url: "wss://ws.gdex.top",
        //     region: "Eastern Asia",
        //     country: "China",
        //     location: "Shanghai",
        //     operator: "Blockproducer: gdex-blockproducer",
        //     contact: "telegram:BrianZhang"
        // },
        // {
        //     url: "wss://api.dxp.dxppp.io:10100",
        //     region: "Eastern Asia",
        //     country: "China",
        //     location: "Hangzhou",
        //     operator: "Blockproducer: dxppp-blockproducer",
        //     contact: "telegram:dxpplusplus"
        // },
        // {
        //     url: "wss://api.dxp.mobi/ws",
        //     region: "Northern America",
        //     country: "U.S.A.",
        //     location: "Virginia",
        //     operator: "Blockproducer: in.abit",
        //     contact: "telegram:abitmore"
        // },
        // {
        //     url: "wss://dxpws.roelandp.nl/ws",
        //     region: "Northern Europe",
        //     country: "Finland",
        //     location: "Helsinki",
        //     operator: "Blockproducer: roelandp",
        //     contact: "telegram:roelandp"
        // },
        // {
        //     url: "wss://api.dxpchain.bhuz.info/ws",
        //     region: "Northern America",
        //     country: "Canada",
        //     operator: "Blockproducer: bhuz",
        //     contact: "telegram:bhuzor"
        // },
        // {
        //     url: "wss://api.dxpgo.net/ws",
        //     region: "Southeastern Asia",
        //     location: "Singapore",
        //     operator: "Blockproducer: xn-delegate",
        //     contact: "wechat:Necklace"
        // },
        // {
        //     url: "wss://dxp.open.icowallet.net/ws",
        //     region: "Eastern Asia",
        //     country: "China",
        //     location: "Hangzhou",
        //     operator: "Blockproducer: magicwallet.blockproducer",
        //     contact: "telegram:plus_wave"
        // },
        // // TODO the owner said it's temporarily closed. Recheck later.
        // //{
        // //    url: "wss://api.dxp.ai",
        // //    region: "Eastern Asia",
        // //    country: "China",
        // //    location: "Beijing",
        // //    operator: "Blockproducer: blockproducer.hiblockchain",
        // //    contact: "telegram:vianull;wechat:strugglingl"
        // //},
        // {
        //     url: "wss://api.61dxp.com",
        //     region: "Eastern Asia",
        //     country: "China",
        //     location: "Shandong",
        //     operator: "Blockproducer: liuye",
        //     contact: "email:work@akawa.ink"
        // },
        // {
        //     url: "wss://api-us.61dxp.com",
        //     region: "Northern America",
        //     country: "USA",
        //     location: "St. Louis",
        //     operator: "Blockproducer: liuye",
        //     contact: "email:work@akawa.ink"
        // },
        // {
        //     url: "wss://api.dex.trading/",
        //     region: "Western Europe",
        //     country: "France",
        //     location: "Paris",
        //     operator: "Blockproducer: zapata42-blockproducer",
        //     contact: "telegram:Zapata_42"
        // },
        // {
        //     url: "wss://api.dxpchain.org/ws",
        //     region: "Western Europe",
        //     country: "France",
        //     location: "",
        //     operator: "dxpchain.org",
        //     contact: ""
        // },
        // {
        //     url: "wss://us.api.dxpchain.org/ws",
        //     region: "Western Europe",
        //     country: "France",
        //     location: "",
        //     operator: "dxpchain.org",
        //     contact: ""
        // },
        // {
        //     url: "wss://asia.api.dxpchain.org/ws",
        //     region: "Western Europe",
        //     country: "France",
        //     location: "",
        //     operator: "dxpchain.org",
        //     contact: ""
        // },
        // {
        //     url: "wss://eu.nodes.dxpchain.ws",
        //     region: "Western Europe",
        //     country: "Germany",
        //     location: "Nuremberg",
        //     operator: "Blockproducer: blocksights",
        //     contact: "telegram:blocksights"
        // },
        // {
        //     url: "wss://api-dxp.liondani.com/ws",
        //     region: "Western Europe",
        //     country: "Germany",
        //     location: "Falkenstein",
        //     operator: "Blockproducer: liondani",
        //     contact: "telegram:liondani"
        // },
        // {
        //     url: "wss://public.xdxp.io/ws",
        //     region: "Western Europe",
        //     country: "Germany",
        //     location: "Nuremberg",
        //     operator: "Blockproducer: xdxpio-wallet",
        //     contact: "telegram: xdxpio"
        // },
        // {
        //     url: "wss://cloud.xdxp.io/ws",
        //     region: "Northern America",
        //     country: "U.S.A.",
        //     location: "VG, Ashburn",
        //     operator: "Blockproducer: xdxpio-wallet",
        //     contact: "telegram: xdxpio"
        // },
        // {
        //     url: "wss://node.xdxp.io/ws",
        //     region: "Western Europe",
        //     country: "Germany",
        //     location: "Falkenstein",
        //     operator: "Blockproducer: xdxpio-wallet",
        //     contact: "telegram: xdxpio"
        // },
        // {
        //     url: "wss://dxp.mypi.win",
        //     region: "Northern America",
        //     country: "U.S.A.",
        //     location: "Seattle, CA",
        //     operator: "Blockproducer: gbac-ety001",
        //     contact: "email:work@akawa.ink"
        // },
        // {
        //     url: "wss://hongkong.dxpchain.im/ws",
        //     region: "East Asia",
        //     country: "China",
        //     location: "Hong Kong",
        //     operator: "Blockproducer: clone",
        //     contact: "telegram: yexiao"
        // },
        // {
        //     url: "wss://singapore.dxpchain.im/ws",
        //     region: "Southeast Asia",
        //     country: "Singapore",
        //     location: "Singapore",
        //     operator: "Blockproducer: clone",
        //     contact: "telegram: yexiao"
        // },
        // {
        //     url: "wss://newyork.dxpchain.im/ws",
        //     region: "Northern America",
        //     country: "U.S.A.",
        //     location: "New York",
        //     operator: "Blockproducer: clone",
        //     contact: "telegram: yexiao"
        // },
        // {
        //     url: "wss://api.dxplebin.com/ws",
        //     region: "Eastern Asia",
        //     country: "China",
        //     location: "Hong Kong",
        //     operator: "Blockproducer: lebin-blockproducer",
        //     contact: "telegram: lebinbit"
        // },
        // // Testnet
        // {
        //     url: "wss://eu.nodes.testnet.dxpchain.ws",
        //     region: "TESTNET - Western Europe",
        //     country: "Germany",
        //     location: "Nuremberg",
        //     operator: "Blockproducer: blocksights",
        //     contact: "telegram:blocksights"
        // },
        // {
        //     url: "wss://testnet.dex.trading/",
        //     region: "TESTNET - Western Europe",
        //     country: "France",
        //     location: "Paris",
        //     operator: "Blockproducer: zapata42-blockproducer",
        //     contact: "telegram:Zapata_42"
        // },
        // {
        //     url: "wss://testnet.xdxp.io/ws",
        //     region: "TESTNET - Europe",
        //     country: "Germany",
        //     location: "Nuremberg",
        //     operator: "Blockproducer: xdxpio-wallet",
        //     contact: "telegram: xdxpio"
        // },
        // {
        //     url: "wss://testnet.dxpchain.im/ws",
        //     region: "Eastern Asia",
        //     country: "Japan",
        //     location: "Tokyo",
        //     operator: "Blockproducer: clone",
        //     contact: "telegram: yexiao"
        // },
        // {
        //     url: "wss://api-testnet.61dxp.com/ws",
        //     region: "Eastern Asia",
        //     country: "China",
        //     location: "Shandong",
        //     operator: "Blockproducer: liuye",
        //     contact: "email:work@akawa.ink"
        // },
        // {
        //     url: "wss://api-us-testnet.61dxp.com/ws",
        //     region: "Northern America",
        //     country: "USA",
        //     location: "St. Louis",
        //     operator: "Blockproducer: liuye",
        //     contact: "email:work@akawa.ink"
        // }
    ],
    ES_WRAPPER_LIST: [
        // {
        //     url: "https://api.dxpchain.ws/openexplorer",
        //     region: "Western Europe",
        //     country: "Germany",
        //     operator: "blocksights.info",
        //     contact: "telegram:blocksights"
        // }
        {
            url: "wss://node.gvxlive.com",
            region: "Western Europe",
            country: "India",
            operator: "blocksights.info",
            contact: "telegram:blocksights"
        }
    ],
    DEFAULT_FAUCET: getFaucet().url,
    TESTNET_FAUCET: getTestFaucet().url
};
