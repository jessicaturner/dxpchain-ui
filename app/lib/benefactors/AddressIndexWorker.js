import {key} from "dxpchainjs";

onmessage = function(event) {
    try {
        console.log("AddressIndexbenefactor start");
        let {pubkeys, address_prefix} = event.data;
        let results = [];
        for (let pubkey of pubkeys) {
            results.push(key.addresses(pubkey, address_prefix));
        }
        postMessage(results);
        console.log("AddressIndexbenefactor done");
    } catch (e) {
        console.error("AddressIndexbenefactor", e);
    }
};
